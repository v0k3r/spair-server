package com.spair.steam.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.converter.json.Jackson2ObjectMapperFactoryBean;

/**
 * Created by vladislav on 22.05.2015.
 */
@Configuration
@ComponentScan(basePackages = {
        "com.spair.steam.service"
})
public class CoreConfig {

        @Bean
        public Jackson2ObjectMapperFactoryBean jackson2ObjectMapperFactoryBean() {
                Jackson2ObjectMapperFactoryBean factoryBean = new Jackson2ObjectMapperFactoryBean();
                return factoryBean;
        }
}
