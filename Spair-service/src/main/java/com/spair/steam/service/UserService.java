package com.spair.steam.service;

import com.spair.steam.dto.UserRegistrationForm;
import com.spair.steam.model.User;

import java.util.List;

/**
 * Created by vladislav on 25.05.2015.
 */
public interface UserService {

    User findUserByLogin(String login);
    User createUser(UserRegistrationForm registrationForm);
    User getById(long id);
    List<User> listAllFollowingsCurrentUser(User currentUser);
}
