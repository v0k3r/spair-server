package com.spair.steam.service;

import com.spair.steam.model.User;

import java.util.List;

/**
 * Created by vladislav on 26.05.2015.
 */
public interface RelationshipService {

    Object create(long target, long current);
    Object destroy(long target, User current);
    List<User> listAllFollowingsCurrentUser(User currentUserId);
    List<User> listAllFollowedByCurrentUser(User currentUserId);
    Object getRelationshipBetween(User followed, User follower);
}
