package com.spair.steam.service.impl;

import com.spair.steam.model.Relationship;
import com.spair.steam.model.User;
import com.spair.steam.repository.RelationshipRepository;
import com.spair.steam.service.RelationshipService;
import com.spair.steam.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * Created by vladislav on 26.05.2015.
 */
@Service
public class RelationshipServiceImpl implements RelationshipService {

    @Autowired
    private RelationshipRepository relationshipRepository;

    @Autowired
    private UserService userService;

    @Override
    @Transactional
    public Object create(long target, long current) {
        if (target == current) {
            //todo переписать, сделать подругому
            return "not allowed";
        }
        Relationship relationship = new Relationship();
        User currentUser = userService.getById(current);
        User targetUser = userService.getById(target);
        Relationship checkRelationship = (Relationship) getRelationshipBetween(targetUser, currentUser);
        if (checkRelationship != null) {
            //это значит что он уже подписан на него
            return "not allowed, you already following this user";
        }
        relationship.setFollowed(/*на кого подписываемся - зафолловленный*/ targetUser);
        relationship.setFollower(/* подписчик - фолловер*/currentUser);
        Relationship relationshipResult = relationshipRepository.save(relationship);
        return relationshipResult;
    }

    @Override
    public Object destroy(long target, User current) {
        if (target == current.getId()){
            return "not allowed";
        }
        User followedUserByCurrent = userService.getById(target);
        Relationship relationship = (Relationship) getRelationshipBetween(followedUserByCurrent, current);
        if (relationship != null)
            relationshipRepository.delete(relationship);
        return true;
    }

    @Override
    public List<User> listAllFollowingsCurrentUser(User currentUserId) {
        return relationshipRepository.listFollowersThisUser(currentUserId);
    }



    @Override
    public List<User> listAllFollowedByCurrentUser(User currentUserId) {
        return relationshipRepository.listFollowedByThisUser(currentUserId);
    }

    @Override
    public Object getRelationshipBetween(User followed, User follower) {
        return relationshipRepository.getRelationshipBetween(followed, follower);
    }

}
