package com.spair.steam.service.impl;

import com.spair.steam.dto.UserRegistrationForm;
import com.spair.steam.model.Relationship;
import com.spair.steam.model.User;
import com.spair.steam.repository.UserRepository;
import com.spair.steam.service.UserService;
import com.spair.steam.util.FormMapper;
import com.spair.steam.util.HibernateUtil;
import org.hibernate.Hibernate;
import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * Created by vladislav on 25.05.2015.
 */
@Service
public class UserServiceImpl implements UserService{

    @Autowired
    private UserRepository userRepository;

    @Override
    public User findUserByLogin(String login) {
        return userRepository.findUserByLogin(login);
    }

    @Override
    public User createUser(UserRegistrationForm registrationForm) {
        User formUser = FormMapper.registrationFormToUser(registrationForm);
        return userRepository.save(formUser);
    }

    @Override
    public User getById(long id) {
        return userRepository.findOne(id);
    }

    @Override
    public List<User> listAllFollowingsCurrentUser(User currentUser) {
        Collection<Relationship> relationships1 = null;
//        try {
//            Session session = HibernateUtil.getSessionFactory().getCurrentSession();
//            relationships1 = currentUser.getFollowings();
//            for (Relationship r : relationships1) System.out.println(r.getId());
//        } catch (Exception e) {
//            System.out.println(e.getMessage());
//        }
        Hibernate.initialize(currentUser.getFollowers().size());
        List<Relationship> relationships = new ArrayList<>(currentUser.getFollowers());

        List<User> users = new ArrayList<>();
        for (Relationship relationship : relationships){
            users.add(relationship.getFollower());
        }
        return users;
    }
}
