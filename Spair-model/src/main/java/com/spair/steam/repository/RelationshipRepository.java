package com.spair.steam.repository;

import com.spair.steam.model.Relationship;
import com.spair.steam.model.User;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by vladislav on 25.05.2015.
 */
@Repository
public interface RelationshipRepository extends CrudRepository<Relationship, Long> {

    /**
     * возвращаем всех пользователей из колонки follower для переданного пользоваетеля из колонки followed
     * то есть всех подписчиков followed
     * @return
     */
    @Query("select r.follower from Relationship r where r.followed = ?1")
    List<User> listFollowersThisUser(User user);

    /**
     * возвращаем всех пользователей на которых подписан данный подьзователь
     * всех кого он зафолловил
     */

    @Query("select r.followed from Relationship r where r.follower = ?1")
    List<User> listFollowedByThisUser(User user);

    @Query("from Relationship r where r.followed=?1 and r.follower=?2")
    Relationship getRelationshipBetween(User followed, User follower);

}
