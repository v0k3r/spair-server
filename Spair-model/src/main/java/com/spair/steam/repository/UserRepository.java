package com.spair.steam.repository;

import com.spair.steam.model.User;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by vladislav on 25.05.2015.
 */
@Repository
public interface UserRepository extends CrudRepository<User, Long> {

    @Query("from User where username = ?1 ")
    User findUserByLogin(String login);
}
