package com.spair.steam.util;

import com.spair.steam.dto.UserRegistrationForm;
import com.spair.steam.model.User;

/**
 * Created by vladislav on 25.05.2015.
 */
public class FormMapper {

    public static User registrationFormToUser(UserRegistrationForm userRegistrationForm){
        User user = new User();
        user.setPassword(userRegistrationForm.getPassword());
        user.setEmail(userRegistrationForm.getEmail());
        user.setEnabled(true);
        user.setUsername(userRegistrationForm.getUsername());
        //avatar and about
        return user;
    }
}
