package com.spair.steam.auth;

import com.spair.steam.model.User;
import com.spair.steam.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.authentication.encoding.ShaPasswordEncoder;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;

import java.util.Arrays;

/**
 * Created by vladislav on 25.05.2015.
 */
@Component("domainAuthProvider")
public class AuthProvider implements AuthenticationProvider {

    private final static String DEFAULT_ROLE = "USER";

    @Autowired
    private ShaPasswordEncoder shaPasswordEncoder;

    @Autowired
    private UserService userService;

    @Autowired
    private TokenService tokenService;

    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {
        Object passwordObj = authentication.getCredentials();
        if (passwordObj == null) {
            passwordObj = "";
        }

        String password = passwordObj.toString();
        String passwordHash = decodePassword(password);
        String login = authentication.getName();
        if (password.isEmpty() || login == null || login.isEmpty()) {
            throw new BadCredentialsException("bad credentional");
        }

        User user = userService.findUserByLogin(login);

        if (user == null) {
            throw new UsernameNotFoundException("User not found");
        }

        if (user.getPassword().equals(passwordHash)) {
            String newToken = tokenService.generateNewToken();
            AuthenticationWithToken tokenAuth = new AuthenticationWithToken(
                    user,
                    null,
                    Arrays.asList(new SimpleGrantedAuthority("ROLE_" + DEFAULT_ROLE)));

            tokenAuth.setToken(newToken);
            SecurityContextHolder.getContext().setAuthentication(tokenAuth);
            tokenService.store(newToken, tokenAuth);
            return tokenAuth;
        } else {
            throw new BadCredentialsException("Bad user password");
        }
    }

    @Override
    public boolean supports(Class<?> authentication) {
        return authentication.equals(UsernamePasswordAuthenticationToken.class);
    }

    private String decodePassword(String password) {
        return shaPasswordEncoder.encodePassword(password, "");
    }
}
