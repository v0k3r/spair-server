package com.spair.steam.auth.ep;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.spair.steam.auth.response.ErrorResponse;
import com.spair.steam.util.WebUtil;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.security.web.access.AccessDeniedHandler;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by vladislav on 25.05.2015.
 */
public class RestAuthenticationEntryPoint implements AuthenticationEntryPoint, AccessDeniedHandler {

    private ObjectMapper objectMapper;

    @Override
    public void handle(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, AccessDeniedException e) throws IOException, ServletException {
        writeError(httpServletResponse);
    }

    @Override
    public void commence(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, AuthenticationException e) throws IOException, ServletException {
        writeError(httpServletResponse);
    }

    private void writeError(HttpServletResponse response) {
        ErrorResponse errorResponse = new ErrorResponse();
        errorResponse.setError("Not allowed for user");
        errorResponse.setMsg("not_allowed");
        WebUtil.objToResponse(errorResponse, response, HttpServletResponse.SC_METHOD_NOT_ALLOWED, objectMapper);
    }

    public void setObjectMapper(ObjectMapper objectMapper) {
        this.objectMapper = objectMapper;
    }
}
