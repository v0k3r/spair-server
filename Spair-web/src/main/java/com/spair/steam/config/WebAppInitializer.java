package com.spair.steam.config;

import org.springframework.core.annotation.Order;
import org.springframework.orm.jpa.support.OpenEntityManagerInViewFilter;
import org.springframework.web.context.request.RequestContextListener;
import org.springframework.web.filter.CharacterEncodingFilter;
import org.springframework.web.filter.DelegatingFilterProxy;
import org.springframework.web.filter.HttpPutFormContentFilter;
import org.springframework.web.servlet.support.AbstractAnnotationConfigDispatcherServletInitializer;

import javax.servlet.Filter;
import javax.servlet.ServletContext;

/**
 * Created by vladislav on 22.05.2015.
 */

@Order(1)
public class WebAppInitializer extends AbstractAnnotationConfigDispatcherServletInitializer {


    @Override
    protected Class<?>[] getRootConfigClasses() {
        return new Class<?>[]{DataSourceConfig.class, PersistenceConfig.class, CoreConfig.class, WebSecurityConfig.class, CachingConfig.class};
    }

    @Override
    protected Class<?>[] getServletConfigClasses() {
        return new Class<?>[] {WebConfig.class};
    }

    @Override
    protected String[] getServletMappings() {
        return new String[]{"/"};
    }

    @Override
    protected Filter[] getServletFilters() {
        DelegatingFilterProxy securityFilterChain = new DelegatingFilterProxy("springSecurityFilterChain");
        CharacterEncodingFilter encodingFilter = new CharacterEncodingFilter();
        OpenEntityManagerInViewFilter inViewFilter = new OpenEntityManagerInViewFilter();
        inViewFilter.setEntityManagerFactoryBeanName("entityManagerFactory");
        encodingFilter.setEncoding("UTF-8");
        encodingFilter.setForceEncoding(true);
//        MultipartFilter multipartFilter=new MultipartFilter();
        return new Filter[]{
                encodingFilter,
                securityFilterChain,
                new HttpPutFormContentFilter(),
                inViewFilter
        };
    }

    @Override
    protected void registerContextLoaderListener(ServletContext servletContext) {
        super.registerContextLoaderListener(servletContext);
        servletContext.addListener(new RequestContextListener());
    }



}
