package com.spair.steam.controller;

import com.spair.steam.auth.TokenService;
import com.spair.steam.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by vladislav on 26.05.2015.
 */
@RequestMapping("/api/v1/")
public class UserController {

    @Autowired
    private TokenService tokenService;

    public @ResponseBody Object getUserByToken(HttpServletRequest request) {
        String token = request.getParameter("token");
        return tokenService.getUserByToken(token);
    }
}
