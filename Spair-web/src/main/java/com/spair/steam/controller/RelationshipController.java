package com.spair.steam.controller;

import com.spair.steam.auth.TokenService;
import com.spair.steam.auth.response.ErrorResponse;
import com.spair.steam.model.Relationship;
import com.spair.steam.model.User;
import com.spair.steam.service.RelationshipService;
import com.spair.steam.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * Created by vladislav on 26.05.2015.
 */
@Controller
@RequestMapping("/api/v1/relationship")
public class RelationshipController {

    /**
     * POST - create relationship
     * DELETE - destroy relationship
     *
     */

    @Autowired
    private RelationshipService relationshipService;

    @Autowired
    private TokenService tokenService;

    @Autowired
    private UserService userService;

    @RequestMapping(method = RequestMethod.POST)
    public @ResponseBody Object follow(@RequestParam(value = "user_id") long userId,
                                       HttpServletRequest request) {
        String token = request.getParameter("token");
        User user =  tokenService.getUserByToken(token);

        Object object =  relationshipService.create(userId, user.getId()); // возможно сразу передвавать юзера, так так его получаем из контекста кеша
        if (object instanceof Relationship) {
            return object;
        }
        else {
            //в текузей версии апи Object это строка
            ErrorResponse err = new ErrorResponse();
            err.setError((String) object);
            err.setMsg("something goes wrong");
            return err;
        }
//        return relationship;
    }


    /**
     * возвращает всех, кто подписан на данного пользователя
     */
    @RequestMapping(method = RequestMethod.GET, value = "/followers")
    public @ResponseBody
    List<User> getAllFollowingsCurrentUser(HttpServletRequest request) {
        User currentUser = tokenService.getUserByToken(request.getParameter("token"));
        return relationshipService.listAllFollowingsCurrentUser(currentUser);
    }

    /**
     * @param request
     * @return возвращает всех на кого подписался текущий пользователь
     */
    @RequestMapping(method = RequestMethod.GET, value = "/followed")
    public @ResponseBody
    List<User> getAllFollowedThisUser(HttpServletRequest request) {
        return relationshipService.listAllFollowedByCurrentUser(tokenService.getUserByToken(request.getParameter("token")));
    }

    @RequestMapping
    public Object unfollow(HttpServletRequest request,
                           @RequestParam(value = "user_id") long userId) {

        User current = tokenService.getUserByToken(request.getParameter("token"));
        return relationshipService.destroy(userId, current);
    }
}
