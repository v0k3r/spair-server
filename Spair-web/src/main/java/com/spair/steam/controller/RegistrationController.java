package com.spair.steam.controller;

import com.spair.steam.dto.UserRegistrationForm;
import com.spair.steam.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.encoding.ShaPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Created by vladislav on 25.05.2015.
 */
@Controller
@RequestMapping

public class RegistrationController {

    @Autowired
    private UserService userService;

    @Autowired
    private ShaPasswordEncoder shaPasswordEncoder;

    @Autowired
    protected AuthenticationManager authenticationManager;

    @RequestMapping(value = "/registration",method = RequestMethod.POST, produces = "application/json")
    public
    @ResponseBody
    Object registerUser(@RequestBody UserRegistrationForm userRegistrationForm,
                        HttpServletResponse response,
                        HttpServletRequest request) {
        if (isExist(userRegistrationForm)) {
            return new ResponseEntity<>("Username already exists", HttpStatus.CONFLICT);
        }
        if (!isPasswordEquals(userRegistrationForm)) {
            return new ResponseEntity<>("Password confirmation and Password must match ",HttpStatus.BAD_REQUEST);
        }
        userRegistrationForm.setPassword(decodePassword(userRegistrationForm.getPassword()));

        response.setStatus(HttpServletResponse.SC_CREATED);
        return userService.createUser(userRegistrationForm);
    }


    private String decodePassword(String password) {
        return shaPasswordEncoder.encodePassword(password, "");
    }

    private boolean isExist(UserRegistrationForm userRegistrationForm) {
//            errors.rejectValue("login", "login", "this login already in use");
        if (userService.findUserByLogin(userRegistrationForm.getUsername()) != null)
            return true; //user exist
        return false;
    }

    private boolean isPasswordEquals(UserRegistrationForm userRegistrationForm) {
        if (userRegistrationForm.getPassword().equals(userRegistrationForm.getConfirmPassword()))
//            bindingResult.rejectValue("confirmPassword", "password", "password doesn't match");
            return true; //password are equals
        return false;
    }
}
