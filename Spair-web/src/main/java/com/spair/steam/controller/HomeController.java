package com.spair.steam.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * Created by vladislav on 22.05.2015.
 */
@RequestMapping("/api/")
@Controller
public class HomeController {

    class Test {
        private String name;

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }
    }

    @RequestMapping(method = RequestMethod.GET, value = "v1/get")
    public @ResponseBody Test testAuth() {
        Test test = new Test();
        test.setName("1212");
        return test;
    }

    @RequestMapping(method = RequestMethod.GET, value = "get")
    public @ResponseBody Test testNoAuth() {
        Test test = new Test();
        test.setName("1");
        return test;
    }

}
