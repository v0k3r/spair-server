package com.spair.steam.util;

import com.fasterxml.jackson.databind.ObjectMapper;

import javax.servlet.http.HttpServletResponse;

/**
 * Created by vladislav on 25.05.2015.
 */
public class WebUtil {

    public static void objToResponse(Object o, HttpServletResponse httpResponse, int status, ObjectMapper msgMapper) {
        httpResponse.setStatus(status);
        httpResponse.addHeader("Content-Type", "application/json");
        try {
            String aString = msgMapper.writeValueAsString(o);
            httpResponse.getWriter().print(aString);
        } catch (Exception e) {
            try {
                httpResponse.getWriter().print("{\"msg:\": \"Mapper exception\"}");
            } catch (Exception e1) {
                e1.printStackTrace();
            }
        }
    }
}
